package com.example.layoutsperfil;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.widget.TextView;

public class Perfil extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_perfil);

        TextView textView = findViewById(R.id.curso_tecnico_nome);
        SpannableString content = new SpannableString("Endereço de email:");
        content.setSpan(new UnderlineSpan(),0,content.length(),0);
                textView.setText(content);

    }
}
